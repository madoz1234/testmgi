<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// 

Route::group(['prefix' => 'authentication','namespace' => 'API'], function () {
    Route::post('/','LoginController@authentication')->name('authAPI')->middleware('2fa');
    Route::post('/register', 'LoginController@register');
    
    Route::group(['middleware' => 'auth:api'], function () {
    	Route::post('/f2aauth', 'LoginController@f2aauth');
    });
});
