<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Validator;

use App\User;

use Auth;

class LoginController extends Controller
{
    use AuthorizesRequests;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    // /**
    //  * Where to redirect users after login.
    //  *
    //  * @var string
    //  */
    // protected $redirectTo = '/home';

    // *
    //  * Create a new controller instance.
    //  *
    //  * @return void
     
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }
    // 
    
    public function authentication(Request $request){
        // dd($request->all());
        
        $authentication = Auth::attempt(['name' => $request->username, 'password' => $request->password]);
        
        if($authentication){
            $user =     User::find(Auth::user()->id);
            // dd();
            $token = $user->api_token;
            $results = [
                'status' => true,
                'message' => 'success',
                'results' => [
                    'token_type' => 'Bearer',
                    'access_token' => $token
                ]
            ];
            return response()->json($results, 200);
        }else{
            return response()->json([
                    'status' => false,
                    'message' => 'Unauthorized',
                    'results' => null
                ], 401);
        }
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }


    public function register(Request $request)
    {
        // Initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');

        $f2akay = $google2fa->generateSecretKey();

        $validate = $this->validator($request->all());



        if ($validate->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Oops ada yang salah ,pastikan data di input dengan benar',
                'results' => $validate->errors()
            ], 403);
        }

        $user = new User;

        $user->create([
            'name' => $request->name,
            'email' => $request->email,
            'api_token' => str_random(40),
            'password' => bcrypt($request->password),
            'google2fa_secret' => $f2akay
        ]);

        return response([
            'status' => true,
            'message' => 'Registrasi Berhasil',
            'secret_code' => $f2akay
        ]);
    }

    public function validatorf2a(array $data)
    {
        return Validator::make($data, [
            'kode' => 'required',
        ]);
    }

    public function f2aauth(Request $request)
    {
        $validate = $this->validatorf2a($request->all());



        if ($validate->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Oops ada yang salah ,pastikan data di input dengan benar',
                'results' => $validate->errors()
            ], 403);
        }

        $user = User::find(auth()->user()->id);
        
        if($user->google2fa_secret == $request->kode){
            return response()->json([
                'status' => true,
                'message' => 'F2A Berhasil diauthentikasi'
            ], 200);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Unauthorized',
                'results' => null
            ], 401);
        }
    }
}
